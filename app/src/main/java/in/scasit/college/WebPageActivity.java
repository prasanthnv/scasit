package in.scasit.college;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebPageActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        String pageUrl = "http://scasit.in";
        Intent incommingIntent= getIntent();
        final ProgressDialog dialog = new ProgressDialog(WebPageActivity.this);
        dialog.setMessage("Loding...");
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Branch");
        final String branches[] = new String[] {"BCA","BSC CS"};
        if(incommingIntent.hasExtra("page")){
            String page = incommingIntent.getStringExtra("page");
            switch (page){
                case "home":
                    loadUrl("http://scasit.in");
                    break;
                case "study":

                    builder.setTitle("Select Branch");
                    builder.setItems(branches, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(which == 0){
                                loadUrl("http://scasit.in/bcastudy.html");
                            }else{
                                loadUrl("http://scasit.in/bscstudy.html");
                            }
                        }
                    });
                    builder.show();
                    break;
                case "syllabus":
                    builder.setItems(branches, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(which == 0){
                                // which is BCA
                                loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url="  + "http://scasit.in/syllabus/bca.pdf");
                            }else{
                                Toast.makeText(getApplicationContext(),"File not found",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                    builder.show();

                    break;
                case "internal":
                    Toast.makeText(getApplicationContext(),"File not found",Toast.LENGTH_LONG).show();
                    break;
                case "feedback":
                    loadUrl("http://scasit.in/archive.html");
                    break;
                case "contact":
                    loadUrl("http://scasit.in/contact.html");
                    break;
            }
        }


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //do whatever you want with the url that is clicked inside the webview.
                //for example tell the webview to load that url.
                view.loadUrl(url);
                //return true if this method handled the link event
                //or false otherwise
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                dialog.show();
            }

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }




    });

        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
}


public void loadUrl(String url){
    webView.loadUrl(url);
}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

}
