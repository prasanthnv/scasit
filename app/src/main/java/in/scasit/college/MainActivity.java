package in.scasit.college;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }



    public void loadPage(View v){
      Button clicked = (Button) v;
        String page = (String) clicked.getTag();
        Intent intent = new Intent(getApplicationContext(),WebPageActivity.class);
        intent.putExtra("page",page);
        startActivity(intent);

    }
}
